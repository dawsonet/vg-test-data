# VG Test Data #

### What it is: ###
This repo contains some datasets to test out vg (and other methods for calling SVs, really).
All data was simulated using RSVSim (citation needed), and most directories have a README with
relatively precise details on how the various files were generated.

### Who can I contact when it's broken: ###
Contact: eric DOT t DOT dawson <AT> gmail.com