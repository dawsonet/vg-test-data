wgsim=/Users/dawsonet/bioinformatica/samtools-1.2/misc/wgsim
ref=/Users/dawsonet/sandbox/test_data/chr22_mod_50DELS_100BP_each/genome_rearranged.fasta
${wgsim} -d 600 -s 50 -e0 -N 1000000 -1 120 -2 120 -r 0 -R 0 -X 0 $ref chr22_mod_50_100_reads1.fq chr22_mod_50_100_reads2.fq
