#!/usr/bin/python
import argparse
import re
import sys
import random
#import csv

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", dest="vcf", required=True, help="A vcf file to parse and modify for input to vg")
    parser.add_argument("-o", dest="outfile", required=False, help="The file to write the modified vcf output to.")
    parser.add_argument("-r", dest="rand", required=False, help="Replace empty bases with a random base (for use with deletions)", default=None)
    return parser.parse_args()

def rand_base():
	base_list = ["A", "C", "T", "G"]
	return random.sample(base_list, 1)[0]

def modify_vcf(vcf_file, out_file, use_rand=False):
	tmp=[]
	with open(vcf_file, "rB") as vfi:
		for line in vfi:
			if line.startswith("#"):
				tmp.append(line.strip())
			else:
				splits = line.split("\t")
				if "DEL" in splits[4]:
					consensus = re.search( "CONSENSUS=[ACTG]*", splits[7])
					if consensus is not None:
						splits[3] = consensus.group(0).split("=")[1]
					if re.search("[ACTG]", splits[3]) is None:
						splits[3] = rand_base()
					splits[4] = rand_base() if use_rand else ""
				o_line = "\t".join([x.strip() for x in splits])
				tmp.append(o_line)
	if out_file is None:
		for i in tmp:
			print i
	else:
		with open(out_file, "w") as ofi:
			for line in tmp:
				ofi.write(line + "\n")

if __name__ == "__main__":
    args = parse_args()
    if args.rand is not None:
        modify_vcf(args.vcf, args.outfile, True)
    else:
        modify_vcf(args.vcf, args.outfile)
