#!/usr/bin/python
import argparse
import random
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", dest="fasta", help="A fasta file to replace ambiguous nucleotides in.", required=True, type=str)
    return parser.parse_args()

bases = ["A", "T", "C", "G"]
def random_base():
    return random.sample(bases, 1)[0]

if __name__ == "__main__":
    args = parse_args()
    with open(args.fasta, "r") as ffi:
        for line in ffi:
            if line.startswith(">"):
                print line.strip()
            else:
                print "".join([random_base() if c not in bases else c for c in line])
