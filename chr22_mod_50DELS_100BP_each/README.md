Steps to generate files:
1. Pull out chr22 from GCA_000001405.15_GRCh38_genomic.fna (from Ensembl); rename sequence ID to "22" w/ sed --> chr22.fa  
2. Use RSVSim to generate 50 deletions of 100bp apiece.   --> genome_rearranged.fasta + deletions.csv  
3. Use wgsim to simulate reads:  
wgsim -d 600 -s 50 -e0 -N 1000000 -1 120 -2 120 -r 0 -R 0 -X 0 $ref chr22_mod_50_100_reads1.fq chr22_mod_50_100_reads2.fq  
--> chr22_mod_50_100_reads1.fq & chr22_mod_50_100_reads2.fq  
4. BWA mem (default settings, 2 threads) | samtools sort --> chr22_50_100_aligned.bam  
5. samtools index chr22_50_100.bam --> chr22_50_100.bam.bai  
6. delly -g chr22.fa -o deletions_chr22_50_100.vcf --> deletions_chr22_50_10.vcf  
7. bgzip deletions_chr22_50_10.vcf --> deletions_chr22_50_10.vcf.gz  
8. tabix index deletions_chr22_50_10.vcf.gz --> deletions_chr22_50_10.vcf.gz.tbi  

## Appears to work fine
vg construct -r chr22.fa -v del_50_100_aligned.vcf.gz > del50.vg

## However, this fails:
vg construct -m 50 -r chr22.fa -v del_50_100_aligned.vcf.gz > del50.vg


## Fails w/ segfault 11 (out of memory / unaddressed space)
vg index del50.vg
