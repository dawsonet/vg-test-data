#!/bin/bash
timestamp=`date +"%H_%M_%S"`
OUTDIR=tmpdir_${timestamp}
GENOME=$1
SV_TYPE=$2
NUM_SV=$3
SIZE=$4

bwa_index=${GENOME}

if [ ! -d "${OUTDIR}" ]
then mkdir ${OUTDIR}
fi

## Insert structural variants using RSVSim

## Simulate reads using wgsim
#${wgsim} -d 600 -s 50 -e0 -N 1000000 -1 120 -2 120 -r 0 -R 0 -X 0 $ref chr22_mod_50_100_reads1.fq chr22_mod_50_100_reads2.fq
READ_LENGTH=120
ERROR_RATE=0
NUM_READS=10000000
READS_AND_INSERT_SIZE=600
STANDARD_DEV_INSERT_SIZE=50
READ_1_LEN=120
READ_2_LEN=120
READ_FILE_A=reads_${timestamp}_a.fq
READ_FILE_B=reads_${timestamp}_b.fq

## First insert variants into the reference genome. Then
## align using BWA, sort using Samtools, and index using samtools.
## Last, call variants with Delly and remove any in N regions with mod_vcf.py
Rscript --vanilla ./bin/make_vars.R -o ${OUTDIR} -g ${GENOME} --sv_type ${SV_TYPE} -n ${NUM_SV} -s ${SIZE} && \
./bin/wgsim -d ${READS_AND_INSERT_SIZE} -s  ${STANDARD_DEV_INSERT_SIZE} -N ${NUM_READS} -e ${ERROR_RATE} \
    -R 0 -r 0 -X 0 \
    -1 ${READ_1_LEN} -2 ${READ_2_LEN} ${OUTDIR}/genome_rearranged.fasta ${READ_FILE_A} ${READ_FILE_B} && \
./bin/bwa mem -t 2 -a -Y ${bwa_index} ${READ_FILE_A} ${READ_FILE_B} | ./bin/samtools view -bhu - | ./bin/samtools sort -@ 2 -T tmp -o aligned_${timestamp}.bam - && \
./bin/samtools index aligned_${timestamp}.bam #\
#./bin/delly -t DEL -o del.${timestamp}.vcf -g ${GENOME} 

